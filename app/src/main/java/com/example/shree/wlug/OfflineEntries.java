package com.example.shree.wlug;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class OfflineEntries extends AppCompatActivity {

    List<TechnoEntry> employeeList;
    SQLiteDatabase mDatabase;
    ListView listViewEmployees;
    TechnoAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_entries);
       try {
           listViewEmployees = (ListView) findViewById(R.id.listViewEmployees);
           employeeList = new ArrayList<>();
           mDatabase = openOrCreateDatabase(MetaForm.DATABASE_NAME, MODE_PRIVATE, null);

           //this method will display the employees in the list
           showEmployeesFromDatabase();
       }
       catch(Exception ex)
       {
          Toast.makeText(getApplicationContext(),"No data found here",Toast.LENGTH_SHORT).show();
       }
    }

    private void showEmployeesFromDatabase() {

        try {
            Cursor cursorEmployees = mDatabase.rawQuery("SELECT * FROM Technotweet", null);

            //if the cursor has some data
            if (cursorEmployees.moveToFirst()) {
                //looping through all the records
                do {
                    //pushing each record in the employee list
                    employeeList.add(new TechnoEntry(
                            cursorEmployees.getString(0),
                            cursorEmployees.getString(1),
                            cursorEmployees.getString(2),
                            cursorEmployees.getString(3),
                            cursorEmployees.getString(4),
                            cursorEmployees.getString(5),
                            cursorEmployees.getString(6)
                    ));
                } while (cursorEmployees.moveToNext());
            }
            //closing the cursor
            cursorEmployees.close();

            //creating the adapter object
            adapter = new TechnoAdapter(this, R.layout.list_layout_employee, employeeList, mDatabase);

            //adding the adapter to listview
            listViewEmployees.setAdapter(adapter);
        }
        catch (Exception ex)
        {
            Toast.makeText(getApplicationContext(),"No Data found here",Toast.LENGTH_LONG).show();
        }
    }

    }


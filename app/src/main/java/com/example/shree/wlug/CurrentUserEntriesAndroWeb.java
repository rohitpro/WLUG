package com.example.shree.wlug;



import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CurrentUserEntriesAndroWeb extends AppCompatActivity {

    DatabaseReference cref;
    FirebaseAuth auth;
    FirebaseUser user;
    String event;
    RecyclerView recyclerView;
    private DatabaseReference mref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_user_entries_andro_web);
        if (isNetworkAvailable()) {
        try {

                auth = FirebaseAuth.getInstance();
                recyclerView = findViewById(R.id.userentries);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                Intent intent = getIntent();
                event = intent.getExtras().getString("event");
                Toast.makeText(getApplicationContext(), event, Toast.LENGTH_SHORT).show();
                // showData(event);
                user = auth.getCurrentUser();
                cref = FirebaseDatabase.getInstance().getReference("UserEntries");
                //cref = FirebaseDatabase.getInstance().getReference("UserEntries").child(user.getUid()).child(event);
                mref = cref.child(user.getUid()).child(event);
            }catch(NullPointerException ex)
            {
                Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Network is not available",Toast.LENGTH_LONG).show();
        }

    }
@Override
public void onStart()
{
    super.onStart();
    //mref = cref.child(user.getUid()).child(event);
try {
    if(isNetworkAvailable()) {
        FirebaseRecyclerAdapter<UserEntries, EntryHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<UserEntries, EntryHolder>(UserEntries.class, R.layout.userentrylayout, EntryHolder.class, mref) {
            @Override
            protected void populateViewHolder(EntryHolder entryHolder, UserEntries userEntries, int i) {

                entryHolder.setFirstname(userEntries.getFirstname());
                entryHolder.setLastname(userEntries.getLastname());
                entryHolder.setEmail(userEntries.getEmail());
                entryHolder.setEventName(userEntries.getEventName());
                entryHolder.setCollege(userEntries.getCollege());
                entryHolder.setMobno(userEntries.getMobno());
                entryHolder.setAmount(userEntries.getAmount());
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }
    else
    {
        Toast.makeText(getApplicationContext(),"Please check your Internet Connection",Toast.LENGTH_LONG).show();
    }
}
catch (Exception ex)
{
    Toast.makeText(getApplicationContext(),ex.toString(),Toast.LENGTH_LONG).show();

}

}

    public static class EntryHolder extends RecyclerView.ViewHolder
    {

        View mview;
        public EntryHolder(View itemView) {
            super(itemView);
            mview = itemView;
        }

        public void setFirstname(String firstname)
        {
            TextView fname = mview.findViewById(R.id.fname);
            fname.setText("First Name : "+ firstname);
        }


        public void setLastname(String lastname)
        {
            TextView lname = mview.findViewById(R.id.lname);
            lname.setText("Last Name :"+ lastname);
        }

        public void setEmail(String email)
        {
            TextView Email = mview.findViewById(R.id.email);
            Email.setText("Email :"+ email);
        }

        public void setMobno(String mobno)
        {
            TextView Mob = mview.findViewById(R.id.mob);
            Mob.setText("MobNo :"+ mobno);
        }

        public void setEventName(String eventName)
        {
            TextView Event = mview.findViewById(R.id.event);
            Event.setText("Event :"+ eventName);
        }

        public void setCollege(String college)
        {
            TextView College = mview.findViewById(R.id.college);
            College.setText("College :"+ college);
        }

        public void setAmount(String amount)
        {
            TextView Amount = mview.findViewById(R.id.amount);
            Amount.setText("Amount :"+ amount);
        }


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}

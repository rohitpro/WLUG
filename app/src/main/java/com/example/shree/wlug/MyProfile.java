package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfile extends AppCompatActivity {

    FirebaseAuth mAuth;
    TextView nameText;
    TextView emailText;
    TextView mobileText;
    String name;
    String email;
    String mobileno;
    ProgressDialog dialog;
    CircleImageView image;
    DatabaseReference database;
    DatabaseReference firstname;
    DatabaseReference Mob;
    DatabaseReference photourl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        nameText = (TextView)findViewById(R.id.name);
        emailText =(TextView) findViewById(R.id.email);
        mobileText =(TextView) findViewById(R.id.mobileno);
        image= (CircleImageView) findViewById(R.id.photourl);
        mAuth=FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        final Handler handler = new Handler(){
            public void handleMessage(Message msg)
            {
                super.handleMessage(msg);
                dialog.incrementProgressBy(20);
            }
        };

        try {


           dialog = new ProgressDialog(MyProfile.this);
           dialog.setTitle("WLUG");
           dialog.setMessage("Generating Profile please wait...");
           dialog.setIcon(R.drawable.logo1);
           dialog.setMax(100);
           dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
           dialog.show();

           email = currentUser.getEmail();
            emailText.setText("Email:" + " " + email);
            database = FirebaseDatabase.getInstance().getReference("WlugUsers");
            firstname = database.child(currentUser.getUid()).child("First name");
            Mob = database.child(currentUser.getUid()).child("Mob No");
            photourl= database.child(currentUser.getUid()).child("PhotoUrl");

           if(firstname == null && Mob == null) {
                name = email.substring(0, email.indexOf('@'));
                nameText.setText("Name:" + " " + name);
                mobileText.setText("Mobile No:" + " " + "+91"+"9999999999");
           }
           else {
               Mob.addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       mobileno = dataSnapshot.getValue().toString();
                       mobileText.setText("Mobile No:" + " "+ mobileno);
                       //Toast.makeText(getApplicationContext(),mobileno,Toast.LENGTH_LONG).show();

                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });

               firstname.addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       name=dataSnapshot.getValue().toString();
                        nameText.setText("Name:" + " " + name);  

                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });

               photourl.addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                        dialog.dismiss();
                       Picasso.with(getApplicationContext()).load(dataSnapshot.getValue().toString()).fit().into(image);
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });

           }
           //url=currentUser.getPhotoUrl();


       }
       catch(NullPointerException ex)
       {
           ex.printStackTrace();
       }
        //Toast.makeText(getApplicationContext(),url.toString(),Toast.LENGTH_LONG).show();
    }

}

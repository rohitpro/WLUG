package com.example.shree.wlug;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import static com.example.shree.wlug.R.*;

public class ClubServices extends AppCompatActivity {

    WebView webView;
    ProgressDialog dialog;
    final Activity activity=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(layout.activity_club_services);
        webView = (WebView)findViewById(R.id.clubservices);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {

                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"WLUG");
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Toast.makeText(getApplicationContext(),"Downloading",Toast.LENGTH_LONG).show();
            }
        });
        dialog = new ProgressDialog(this);
        webView.setWebChromeClient(new WebChromeClient(){
           public void onProgressChanged(WebView view,int progress)
           {
               dialog.setTitle("WLUG");
               dialog.setIcon(drawable.logo1);
               dialog.setMessage("Loading...");
               dialog.setProgress(progress * 100);
               dialog.show();
               if(progress == 100)
                   dialog.dismiss();
                   activity.setTitle("WLUG ClubServices");
           }
        });


        webView.setWebViewClient(new WebViewClient(){

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl("http://wcewlug.org/wcewlug/clubservice.php");

    }

    @Override
    public void onStart()
    {
        super.onStart();
        /*dialog = new ProgressDialog(this);
        dialog.setIcon(drawable.logo1);
        dialog.setTitle("Club Services");
        dialog.setMessage("Loading Clubservices");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //dialog.setCanceledOnTouchOutside(false);
        dialog.show();*/
    }
}

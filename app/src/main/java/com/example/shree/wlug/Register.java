package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity {

    FirebaseAuth mAuth;
    EditText emailEdit;
    EditText passwordEdit;
    EditText vKey;
    DatabaseReference database;
    //DatabaseReference databaseReference;
    String email,password;
    String member,alumni,guest;
    Button register;
    TextInputLayout errorEmail;
    TextInputLayout errorPassword;
    TextInputLayout erroKey;
    ProgressDialog dialog;
    final String AlumniCode = "WLUG@Alumni";
    final String MemberCode = "WLUG@Member";
    int flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        emailEdit = (EditText) findViewById(R.id.email);
        passwordEdit = (EditText) findViewById(R.id.password);
        vKey = (EditText) findViewById(R.id.v_key);
        register= (Button) findViewById(R.id.register);
        erroKey = (TextInputLayout) findViewById(R.id.key);
        errorEmail= (TextInputLayout) findViewById(R.id.errorEmail);
        errorPassword= (TextInputLayout) findViewById(R.id.errorPassword);
        Intent intent = getIntent();
        member=intent.getExtras().getString("member");
        guest=intent.getExtras().getString("guest");
        alumni=intent.getExtras().getString("alumni");
        database = FirebaseDatabase.getInstance().getReference("WlugUsers");
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Register.this.finish();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
    public void onStart()
    {
        super.onStart();
        try {
            if (member.equals("WLUG Member")) {
                vKey.setEnabled(true);
            } else if (alumni.equals("WLUG Alumni")) {
                vKey.setEnabled(true);
            } else {
                vKey.setEnabled(false);
                vKey.setVisibility(View.INVISIBLE);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }
    public void register(View view)
    {
        showProgress();
        email = emailEdit.getText().toString();
        password = passwordEdit.getText().toString();
        String vkey=vKey.getText().toString().trim();
        if(email.isEmpty() || password.isEmpty())
        {
            if(email.isEmpty()) {
                // Toast.makeText(MainActivity.this, "All Fields Are Required", Toast.LENGTH_SHORT).show();
                errorEmail.setError("Email is required.");
            }
            if(password.isEmpty()) {
                //Toast.makeText(MainActivity.this, "All Fields Are Required", Toast.LENGTH_SHORT).show();
                errorPassword.setError("Password is required.");
            }

            if(vkey.isEmpty())
            {
                erroKey.setError("Key is required");
            }
            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            {
                dialog.dismiss();
                emailEdit.setError("Invalid Email");
                emailEdit.setFocusable(true);
            }
            dialog.dismiss();

        }
        else {
                if(guest.equals("Guest User"))
                {
                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(getApplicationContext(),task.toString(),Toast.LENGTH_LONG).show();
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Successfully Registered.", Toast.LENGTH_SHORT).show();
                                FirebaseUser user = mAuth.getCurrentUser();
                                database.child(user.getUid()).child("flag").setValue(1);
                                database.child(user.getUid()).child("email").setValue(email);

                                database.child(user.getUid()).child("First name").setValue("");

                                database.child(user.getUid()).child("Last name").setValue("");

                                database.child(user.getUid()).child("Mob No").setValue("");

                                database.child(user.getUid()).child("Address").setValue("");
                                database.child(user.getUid()).child("PhotoUrl").setValue("https://firebasestorage.googleapis.com/v0/b/wlug-d12d5.appspot.com/o/UserImages%2FDefaultUserImage%2FUser.png?alt=media&token=bc7dff13-b107-48fc-907b-9202da305542");
                                //database.child(user.getUid()).child("password").setValue(password);
                                database.child(user.getUid()).child("Type").setValue("Guest");
                                boolean verified = user.isEmailVerified();
                                if (verified) {
                                    Toast.makeText(getApplicationContext(), "Email Verified", Toast.LENGTH_LONG).show();
                                } else {
                                    //Toast.makeText(getApplicationContext(), "Email not Verified", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                    user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            Toast.makeText(getApplicationContext(), "Email sent to your Gmail Account. Please verify your Email", Toast.LENGTH_LONG).show();

                                            Register.this.finish();
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class));

                                        }
                                    });
                                }
                            } else {
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else if (member.equals("WLUG Member"))
                {
                    if(vkey.equals(MemberCode)) {
                        //showProgress();
                        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(getApplicationContext(), task.toString(), Toast.LENGTH_LONG).show();
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Successfully Registered.", Toast.LENGTH_SHORT).show();
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    database.child(user.getUid()).child("flag").setValue(2);
                                    database.child(user.getUid()).child("email").setValue(email);
                                    //database.child(user.getUid()).child("password").setValue(password);
                                    database.child(user.getUid()).child("Type").setValue("Member");
                                    database.child(user.getUid()).child("First name").setValue("");

                                    database.child(user.getUid()).child("Last name").setValue("");

                                    database.child(user.getUid()).child("Mob No").setValue("");

                                    database.child(user.getUid()).child("Address").setValue("");
                                    database.child(user.getUid()).child("PhotoUrl").setValue("https://firebasestorage.googleapis.com/v0/b/wlug-d12d5.appspot.com/o/UserImages%2FDefaultUserImage%2FUser.png?alt=media&token=bc7dff13-b107-48fc-907b-9202da305542");

                                    boolean verified = user.isEmailVerified();

                                    if (verified) {
                                        Toast.makeText(getApplicationContext(), "Email Verified", Toast.LENGTH_LONG).show();
                                    } else {

                                        Toast.makeText(getApplicationContext(), "Email not Verified", Toast.LENGTH_LONG).show();
                                        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                dialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Email sent to your Gmail Account. Please verify your Email", Toast.LENGTH_LONG).show();
                                                Register.this.finish();
                                                startActivity(new Intent(getApplicationContext(), MainActivity.class));

                                            }
                                        });
                                    }
                                } else {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    else
                    {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Please check your Verification Key",Toast.LENGTH_LONG).show();

                    }
                }

                else if (alumni.equals("WLUG Alumni"))
                {
                    if(vkey.equals(AlumniCode)) {
                        //showProgress();
                        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(getApplicationContext(), task.toString(), Toast.LENGTH_LONG).show();
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Successfully Registered.", Toast.LENGTH_SHORT).show();
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    database.child(user.getUid()).child("flag").setValue(3);
                                    database.child(user.getUid()).child("email").setValue(email);
                                    //database.child(user.getUid()).child("password").setValue(password);
                                    database.child(user.getUid()).child("Type").setValue("Alumni");
                                    database.child(user.getUid()).child("First name").setValue("");

                                    database.child(user.getUid()).child("Last name").setValue("");

                                    database.child(user.getUid()).child("Mob No").setValue("");

                                    database.child(user.getUid()).child("Address").setValue("");
                                    database.child(user.getUid()).child("PhotoUrl").setValue("https://firebasestorage.googleapis.com/v0/b/wlug-d12d5.appspot.com/o/UserImages%2FDefaultUserImage%2FUser.png?alt=media&token=bc7dff13-b107-48fc-907b-9202da305542");

                                    boolean verified = user.isEmailVerified();
                                    if (verified) {
                                        Toast.makeText(getApplicationContext(), "Email Verified", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Email not Verified", Toast.LENGTH_LONG).show();
                                        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                dialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Email sent to your Gmail Account. Please verify your Email", Toast.LENGTH_LONG).show();
                                                Register.this.finish();
                                                startActivity(new Intent(getApplicationContext(), MainActivity.class));

                                            }
                                        });
                                    }
                                } else {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else{
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Please check your Verification Key",Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Please check your Verification Key",Toast.LENGTH_LONG).show();
                }

        }






    }
    public void showProgress()
    {
        dialog = new ProgressDialog(this);
        dialog.setIcon(R.drawable.logo1);
        dialog.setTitle("WLUG");
        dialog.setMessage("Registering Please wait...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

}

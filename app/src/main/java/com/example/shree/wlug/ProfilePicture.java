package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilePicture extends AppCompatActivity {

    Button choose,upload;
    CircleImageView imageView;
    StorageReference storage;
    FirebaseAuth mAuth;
    StorageReference sref;
    Bitmap bitmap;
    FirebaseUser user;
    FirebaseDatabase dref;
    DatabaseReference mref;
    ProgressDialog dialog;
    Uri uri;
    public static  final int REQUEST_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture);
        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        mref = FirebaseDatabase.getInstance().getReference("WlugUsers");
        choose = (Button) findViewById(R.id.chooseimage);
        upload =(Button) findViewById(R.id.uploadimage);
        imageView = (CircleImageView) findViewById(R.id.imageView);
        storage = FirebaseStorage.getInstance().getReference();

    }

    /*public String getStringImage(Bitmap bmp)
    {
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] imageBytes=baos.toByteArray();
        String encodeImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return encodeImage;
    }*/

    public void chooseImage(View view)
    {
        Intent intent =new Intent(Intent.ACTION_VIEW);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent chooser = Intent.createChooser(intent,"Gallery");
        startActivityForResult(chooser,REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestcode,int resultcode,Intent data)
    {
        super.onActivityResult(requestcode,resultcode,data);
        uri=data.getData();
        if(requestcode==REQUEST_CODE && resultcode==RESULT_OK && data!=null && data.getData()!=null)
        {
            try
            {
                bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                imageView.setImageBitmap(bitmap);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }                                   
    }


    public void uploadImage(View view)
    {
        showProgress();
        StorageReference iref = storage.child("UserImages").child(user.getUid()).child(uri.getLastPathSegment());
          //String image = getStringImage(bitmap);
        iref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Uploaded Successfully",Toast.LENGTH_LONG).show();
                mref.child(user.getUid()).child("PhotoUrl").setValue(taskSnapshot.getDownloadUrl().toString());
                startActivity(new Intent(getApplicationContext(),HomeActivity.class));

            }
        });

        //Toast.makeText(getApplicationContext(),iref.getDownloadUrl().toString(),Toast.LENGTH_LONG).show();
    }

    public void showProgress()
    {
        dialog = new ProgressDialog(this);
        dialog.setIcon(R.drawable.logo1);
        dialog.setTitle("WLUG");
        dialog.setMessage("Uploading Please wait...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

}

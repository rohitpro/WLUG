package com.example.shree.wlug;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class AboutEvent extends AppCompatActivity {

    private WebView webView;
    private ProgressDialog dialog;
    final Activity activity=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_event);


        webView = (WebView)findViewById(R.id.aboutEvent);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        dialog = new ProgressDialog(this);
        webView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view,int progress)
            {
                dialog.setTitle("WLUG");
                dialog.setIcon(R.drawable.logo1);
                dialog.setMessage("Loading...");
                dialog.setProgress(progress * 100);
                dialog.show();
                if(progress == 100)
                    dialog.dismiss();
                activity.setTitle("TechnoTweet");
            }
        });


        webView.setWebViewClient(new WebViewClient(){

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl("http://wcewlug.org/TechnoTweet/index.html");
    }
}

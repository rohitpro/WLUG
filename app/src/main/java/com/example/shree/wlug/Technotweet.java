package com.example.shree.wlug;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class Technotweet extends Fragment {


    Button techoRegister;
    private DatabaseReference mref;
    FirebaseUser user;
    int flag;
    private FirebaseAuth auth;

    public Technotweet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_technotweet, container, false);
        techoRegister = (Button) view.findViewById(R.id.technoRegister);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        mref = FirebaseDatabase.getInstance().getReference("WlugUsers");
        mref.child(user.getUid()).child("flag").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                flag = Integer.parseInt(dataSnapshot.getValue().toString());
                // Toast.makeText(getApplicationContext(),flag,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        techoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(flag == 1)
                {
                    techoRegister.setEnabled(false);
                    Toast.makeText(getContext(),"Access Denied!",Toast.LENGTH_SHORT).show();
                    Snackbar.make(view,"Permission denied to register",Snackbar.LENGTH_LONG).setDuration(5000).show();
                }
                else if(flag ==3){

                    techoRegister.setEnabled(false);
                    Toast.makeText(getContext(),"Access Denied!",Toast.LENGTH_SHORT).show();
                    Snackbar.make(view,"Permission denied to register",Snackbar.LENGTH_LONG).setDuration(5000).show();
                }
                else
                {
                    startActivity(new Intent(getContext(),MetaForm.class));

                }
            }
        });
        return view;
    }


}

package com.example.shree.wlug;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class VisitWebsite extends AppCompatActivity {

    WebView webView;
    final Activity activity = this;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_visit_website);
        webView = findViewById(R.id.viewsite);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        dialog = new ProgressDialog(this);
        webView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view,int progress)
            {
                dialog.setTitle("WLUG");
                dialog.setIcon(R.drawable.logo1);
                dialog.setMessage("Loading...");
                dialog.setProgress(progress * 100);
                dialog.show();
                if(progress == 100)
                    dialog.dismiss();
                    activity.setTitle("WLUG");
            }

        });


        webView.setWebViewClient(new WebViewClient(){

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl("http://wcewlug.org/wcewlug/");

    }
}

package com.example.shree.wlug;
import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
public class DiscussionForum extends AppCompatActivity {

    TabLayout tb;
    ViewPager tl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_forum);
        tb= (TabLayout) findViewById(R.id.tl);
        tb.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.tabcolor)));
        tl= (ViewPager) findViewById(R.id.vp);
        MyAdapter adapter=new MyAdapter(getSupportFragmentManager());
        tl.setAdapter(adapter);
        tb.setupWithViewPager(tl);
    }

    class MyAdapter extends FragmentStatePagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);

        }


        @Override
        public Fragment getItem(int i) {
            Fragment f=null;

            if(i==0)
            {
                f=new Discussion();
            }
            if(i==1)
            {
                f=new ClubServiceDiscussion();
            }
            return f;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String name=null;
            if(position==0)
            {
                name="Technical Discussion";
            }
            if(position==1)
            {
                name="Club Service Discussion";
            }

            return name;
        }
    }
}

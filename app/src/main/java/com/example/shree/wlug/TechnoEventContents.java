package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

public class TechnoEventContents extends AppCompatActivity {

    Button android, web;
   // StorageReference Sref;
    FirebaseStorage sref;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_techno_event_contents);

        sref = FirebaseStorage.getInstance();

        android = findViewById(R.id.android);
        web = findViewById(R.id.web);

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar.make(v,"No contents are found",Snackbar.LENGTH_LONG).show();
            }
        });
        android.setOnClickListener(new View.OnClickListener() {
            Handler handler = new Handler(){
                public void handleMessage(Message msg)
                {
                    super.handleMessage(msg);
                    dialog.incrementProgressBy(10);
                }
            };
            @Override
            public void onClick(View v) {

                dialog = new ProgressDialog(TechnoEventContents.this);
                dialog.setTitle("WLUG");
                dialog.setMessage("Uploading please wait...");
                dialog.setIcon(R.drawable.logo1);
                dialog.setMax(100);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dialog.setCancelable(false);
                dialog.show();
                StorageReference ref = sref.getReferenceFromUrl("gs://wlug-d12d5.appspot.com/Event_Contents/AndroBeatz/TechnoTweet 2K18.pdf");
                File rootPath = new File(Environment.getExternalStorageDirectory(), "WLUG");
                if(!rootPath.exists()) {
                    rootPath.mkdirs();
                }
                final File localFile = new File(rootPath,"TechnoAndroidContent.pdf");

                ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                try {
                                    while (dialog.getProgress() <= dialog.getMax()) {
                                        Thread.sleep(200);
                                        handler.sendMessage(handler.obtainMessage());
                                        if (dialog.getProgress() == dialog.getMax()) {
                                            dialog.dismiss();

                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }



                            }
                        }).start();

                        Toast.makeText(getApplicationContext(),"File has been downloded at" +localFile.toString(),Toast.LENGTH_LONG).show();
                        //  updateDb(timestamp,localFile.toString(),position);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("firebase ",";local tem file not created  created " +exception.toString());
                    }
                });
            }
        });
    }
}

package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.onesignal.OneSignal;

public class MainActivity extends AppCompatActivity {
    Button register;
    TextView forget;
    AdView adView;
    FirebaseAuth mAuth;
    ProgressDialog pdg;
    EditText emailEdit;
    AlertDialog dialog;
    EditText passwordEdit;
    TextView member,alumni,guest;
    String email,password;
    Button login;
    View view;
    TextInputLayout errorEmail;
    TextInputLayout errorPassword;
    ProgressDialog dialog1;
    FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        register = (Button) findViewById(R.id.createnew);
        forget = (TextView) findViewById(R.id.forgetpassword);
        login= (Button) findViewById(R.id.loginId);
        member= (TextView) findViewById(R.id.member);
        alumni = (TextView) findViewById(R.id.alumni);
        guest = (TextView) findViewById(R.id.guest);
        MobileAds.initialize(this,"ca-app-pub-3393340390341566/3155836879");
        adView= (AdView) findViewById(R.id.adView);
        emailEdit = (EditText) findViewById(R.id.email);
        passwordEdit = (EditText) findViewById(R.id.password);
        String token =FirebaseInstanceId.getInstance().getToken();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        errorEmail= (TextInputLayout) findViewById(R.id.errorEmail);
        errorPassword= (TextInputLayout) findViewById(R.id.errorPassword);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        mAuth = FirebaseAuth.getInstance();
         currentUser = mAuth.getCurrentUser();
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        if(isNetworkAvailable())
        {
            Toast.makeText(getApplicationContext(),"Internet Connection is available",Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Check Your Internet Connection",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        try {
            currentUser = mAuth.getCurrentUser();
            boolean verified = currentUser.isEmailVerified();
            if (!verified) {
                Snackbar.make(view, "Please Verify Your Email", Snackbar.LENGTH_LONG).show();
            } else {
                if (currentUser != null) {
                    startActivity(new Intent(MainActivity.this, HomeActivity.class));

                } else {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    // Toast.makeText(getApplicationContext(),"Please verify your Email.",Toast.LENGTH_LONG).show();
                }
            }
        }
        catch (Exception e)
        {
            //Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
        }
    }
@Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finishAffinity();

    }

    public void loginUser()
    {
        email = emailEdit.getText().toString();
        password = passwordEdit.getText().toString();
        if(email.isEmpty() || password.isEmpty())
        {
            if(email.isEmpty()) {
               // Toast.makeText(MainActivity.this, "All Fields Are Required", Toast.LENGTH_SHORT).show();
                errorEmail.setError("Email is required.");
            }
            if(password.isEmpty()) {
                //Toast.makeText(MainActivity.this, "All Fields Are Required", Toast.LENGTH_SHORT).show();
                errorPassword.setError("Password is required.");
            }
            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            {
                emailEdit.setError("Invalid Email");
                emailEdit.setFocusable(true);
            }

        }
        else {
            dialog1 = new ProgressDialog(this);
            dialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog1.setIcon(R.drawable.logo1);
            dialog1.setTitle("WLUG");
            dialog1.setMessage("Authenticating Please wait...");
            dialog1.show();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                //Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                boolean verified = user.isEmailVerified();
                                if (verified) {
                                    dialog1.dismiss();
                                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                    Toast.makeText(getApplicationContext(), "Verified", Toast.LENGTH_LONG).show();

                                } else {
                                    dialog1.dismiss();
                                    Toast.makeText(getApplicationContext(), "Please verify your email", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                // If sign in fails, display a message to the user.
                                // Log.w(TAG, "signInWithEmail:failure", task.getException());
                                dialog1.dismiss();
                                Toast.makeText(MainActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                //updateUI();
                            }

                            // ...
                        }
                    });
        }
    }

    public void registerUser()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
       View mview = getLayoutInflater().inflate(R.layout.registertype, null);
       dialog = builder.create();
        dialog.setView(mview);
        dialog.show();
    }

    public void forgetPassword(View view)
    {

        startActivity(new Intent(this,Forget.class));
    }

    public void memberRegistration(View view)
    {
        dialog.dismiss();
        dialog.cancel();
        String Member = "WLUG Member";
        String Alumni = "";
        String Guest = "";
        Intent intent = new Intent(getApplicationContext(),Register.class);
        intent.putExtra("member",Member);
        intent.putExtra("alumni",Alumni);
        intent.putExtra("guest",Guest);
        startActivity(intent);
    }

    public void guestRegistration(View view)
    {
        dialog.dismiss();
        dialog.cancel();
        String Member = "guest";
        String Alumni = "guest";
        String Guest = "Guest User";
        Intent intent = new Intent(getApplicationContext(),Register.class);
        intent.putExtra("member",Member);
        intent.putExtra("alumni",Alumni);
        intent.putExtra("guest",Guest);
        startActivity(intent);
    }

    public void alumniRegistration(View view)
    {
        dialog.dismiss();
        dialog.cancel();
        String Alumni = "WLUG Alumni";
        String Member = "";
        String Guest = "";
        Intent intent = new Intent(getApplicationContext(),Register.class);
        intent.putExtra("alumni",Alumni);
        intent.putExtra("member",Member);
        intent.putExtra("guest",Guest);
        startActivity(intent);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}



package com.example.shree.wlug;

/**
 * Created by rohit on 2/2/18.
 */

public class TechnoEntry
{

    String name, email, mob_no, Event,EnteredBy, Amount, CollegeName;

    public TechnoEntry(String name, String email, String mob_no, String event, String enteredBy, String amount, String collegeName) {
        this.name = name;
        this.email = email;
        this.mob_no = mob_no;
        this.Event = event;
        this.EnteredBy = enteredBy;
        this.Amount = amount;
        this.CollegeName = collegeName;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getMob_no() {
        return mob_no;
    }

    public String getEvent() {
        return Event;
    }

    public String getEnteredBy() {
        return EnteredBy;
    }

    public String getAmount() {
        return Amount;
    }

    public String getCollegeName() {
        return CollegeName;
    }
}

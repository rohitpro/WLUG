package com.example.shree.wlug;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rohit on 13/1/18.
 */

public class sharedpref {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    int PRIVATE_MODE=0;

    /*Title for Shared preference */
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public sharedpref(Context context){
        this.context=context;
        sharedPreferences=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=sharedPreferences.edit();
    }

    public void setFirstLaunch(boolean launch) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH,launch);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH,true);

    }

}

package com.example.shree.wlug;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Metamorphosis extends Fragment {

    Button register;

    public Metamorphosis() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_metamorphosis,container,false);
        register= (Button) view.findViewById(R.id.registerEvent);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://wcewlug.org/metamorphosis2k17/index.html"));
                Intent chooser=Intent.createChooser(intent,"Launch Browser");
                startActivity(chooser);
            }
        });

        return view;
    }

}

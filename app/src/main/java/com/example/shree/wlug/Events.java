package com.example.shree.wlug;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Events extends AppCompatActivity {


    TabLayout tb;
    ViewPager tl;
    Spinner event;
    FirebaseAuth auth;
    ArrayAdapter<String> eventType;
    DatabaseReference mref;
    int flag=0;
    String EventRegister[] = { "Select Workshop","AndroBeatz", "Webino"};
    private static final int REQUEST_RESPONSE = 1;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        tb= (TabLayout) findViewById(R.id.tl);
        tb.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.tabcolor)));
        tl= (ViewPager) findViewById(R.id.vp);
        mref = FirebaseDatabase.getInstance().getReference("WlugUsers");
        MyAdapter adapter=new MyAdapter(getSupportFragmentManager());
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        mref.child(user.getUid()).child("flag").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                flag = Integer.parseInt(dataSnapshot.getValue().toString());
               // Toast.makeText(getApplicationContext(),flag,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        tl.setAdapter(adapter);
        tb.setupWithViewPager(tl);
    }


    class MyAdapter extends FragmentStatePagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fm=null;

            if(i==0)
            {
                fm=new Metamorphosis();
            }
            if(i==1)
            {
                fm=new Technotweet();
            }

            return fm;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title="";
            if(position==0)
            {
                title="Metamorphosis";

            }
            if(position==1)
            {
                title="Technotweeet";
            }
            return title;
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.events,menu);
        if(flag==1) {
            menu.findItem(R.id.entries).setEnabled(false);
            menu.findItem(R.id.ofentries).setEnabled(false);
        }
        else if(flag==3) {
            menu.findItem(R.id.entries).setEnabled(false);
            menu.findItem(R.id.ofentries).setEnabled(false);
        }
        else {

            menu.findItem(R.id.entries).setEnabled(true);
            menu.findItem(R.id.ofentries).setEnabled(true);

        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.contents)
        {

            startActivity(new Intent(getApplicationContext(),TechnoEventContents.class));
             //Toast.makeText(getApplicationContext(),"Updating soon",Toast.LENGTH_LONG).show(); //showEntry();
            //Toast.makeText(getApplicationContext(),"register",Toast.LENGTH_LONG).show();

        }
        if(id==R.id.entries)
        {
            try {

                showEntry();
            }
            catch (Exception ex) {
                Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
            }
        }

        if(id == R.id.ofentries)
        {
                startActivity(new Intent(getApplicationContext(),OfflineEntries.class));
        }
        if(id ==R.id.logout)
        {
            auth.signOut();
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }

        if(id ==R.id.about)
        {
            //Toast.makeText(getApplicationContext(),"Comming Soon",Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),AboutEvent.class));
        }
        if(id ==R.id.exit)
        {
            finish();
            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    public void showEntry()
    {

        //final String[] name = new String[1];
        View view=getLayoutInflater().inflate(R.layout.eventselector,null);
      //  setContentView(view);
        event= (Spinner) view.findViewById(R.id.event);
        eventType=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,EventRegister);
        eventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        event.setAdapter(eventType);
        AlertDialog.Builder builder=new AlertDialog.Builder(Events.this);
        AlertDialog dialog=builder.create();
        dialog.setView(view);
        dialog.show();

       // Toast.makeText(getApplicationContext(), name[0],Toast.LENGTH_LONG).show();
        event.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapterView.getItemAtPosition(i).toString();
               // Toast.makeText(getApplicationContext(), name,Toast.LENGTH_LONG).show();
                if(name=="AndroBeatz")
                {
                   Intent intent=new Intent(Events.this,CompletePackageEntry.class);
                   intent.putExtra("event",name);
                   startActivity(intent);
                }
                if(name=="Webino")
                {
                    Intent intent=new Intent(Events.this,PythonEntry.class);
                    intent.putExtra("event",name);
                    startActivityForResult(intent,REQUEST_RESPONSE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
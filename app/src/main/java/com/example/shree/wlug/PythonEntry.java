package com.example.shree.wlug;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PythonEntry extends AppCompatActivity {

    TextView entry;
    Spinner webEntry;
    View view;
    DatabaseReference cref;
    String name;
    int flag =1;
    String CollegeName[] = {"Select College here","AMGOI","BIMAT","GPTS","ADCES","GPKP","KIT","WLINGDN","DKTE","RIT","GCOEK","DYPK","BVKOP","JJMCOE","DOT","SIT","WCE","GPM","SGI"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_python_entry);
        entry = (TextView) findViewById(R.id.entry);
        webEntry = (Spinner) findViewById(R.id.webCollegeEntries);
        cref=FirebaseDatabase.getInstance().getReference("TechnoTweet");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,CollegeName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        webEntry.setAdapter(adapter);
        showWebEntry();
        webEntry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int c=1;
                while(c < CollegeName.length)
                {
                    if(CollegeName[c].equals(adapterView.getItemAtPosition(i).toString())) {
                            /*ProgressDialog dialog = new ProgressDialog(getApplicationContext());
                            dialog.setTitle(CollegeName[c].toString() + "Total Entries");
                            dialog.setMessage("7");
                            dialog.setIcon(R.drawable.logo1);
                            dialog.setCanceledOnTouchOutside(true);
                            dialog.show();*/
                        cref.child("Webino").child(adapterView.getItemAtPosition(i).toString()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                Toast.makeText(getApplicationContext(),"Total Entry "+ Integer.toString((int)dataSnapshot.getChildrenCount()),Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                    c++;
                }

                // Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        //Intent intent = getIntent();
        //name = intent.getExtras().getString("event");
        //Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
        //showPythonEntry();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.selfentries,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.selfentries)
        {
            if(flag ==1) {

                Intent intent = new Intent(PythonEntry.this, CurrentUserEntriesAndroWeb.class);
                intent.putExtra("event", "Webino");
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "comming soon", Toast.LENGTH_SHORT).show();

                Toast.makeText(getApplicationContext(), "comming soon", Toast.LENGTH_SHORT).show();
            }
            }
        return super.onOptionsItemSelected(item);
    }

    public void showWebEntry()
    {

        DatabaseReference eventEntry = FirebaseDatabase.getInstance().getReference("TechnoTweet");
        DatabaseReference totalCountref = eventEntry.child("Webino");
        totalCountref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int i=0;
                long count =0;
                while(i<CollegeName.length)
                {
                    count += (int) dataSnapshot.child(CollegeName[i]).getChildrenCount();
                    i++;
                }
               // Toast.makeText(getApplicationContext(),"Total Count"+Integer.toString((int) count),Toast.LENGTH_LONG).show();
                entry.setText("Total "+ Integer.toString((int)count) + "Entries");
                //Toast.makeText(getApplicationContext(),Integer.toString((int) count),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Toast.makeText(getApplicationContext(),"Failed to load the data",Toast.LENGTH_LONG).show();
            }
        });


    }


   /* public void showPythonEntry()
    {
        Intent intent = getIntent();
        name = intent.getExtras().getString("event");
       // name="Python";
        Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, EventEntry.fetchurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                       // Snackbar.make(view, response, Snackbar.LENGTH_LONG).show();
                        entry.setText(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(EventEntry.EventName, name);
                return params;
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }*/


}

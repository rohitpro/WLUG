package com.example.shree.wlug;

import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by rohit on 18/1/18.
 */

public class MyFirebaseInstanceId extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        //now we will have the token
        String token = FirebaseInstanceId.getInstance().getToken();
        Toast.makeText(getApplicationContext(),token,Toast.LENGTH_LONG).show();
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {


    }
}

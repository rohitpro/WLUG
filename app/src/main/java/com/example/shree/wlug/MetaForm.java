package com.example.shree.wlug;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MetaForm extends AppCompatActivity {

    Spinner spinner;
    Button submit;
    Spinner college;
    String EventRegister[] = { "AndroBeatz", "Webino"};
    ArrayAdapter<String> eventType;
    String CollegeName[] = {"AMGOI","BIMAT","GPTS","ADCES","GPKP","KIT","WLINGDN","DKTE","RIT","GCOEK","DYPK","BVKOP","JJMCOE","DOT","SIT","WCE","S.Bhokare","GPM","SGI"};
    ArrayAdapter<String> collegeType;
    RadioGroup group;
    RadioButton male,female;
    EditText firstname,lastname,emailid,mobno,amount;
    RadioGroup Osgroup;
    RadioButton linux,windows;
    FirebaseAuth mauth;
    DatabaseReference eventref,androref;
    SQLiteDatabase database;
    public static final String DATABASE_NAME = "myemployeedatabase";
    ProgressDialog dialog;
    String email,name;
    int remainingAmount=0;
    final int AndroAmount = 300;
    final int WebAmount = 300;
    FirebaseUser user;
    private DatabaseReference UserEntriesref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_meta_form);
            Osgroup = (RadioGroup) findViewById(R.id.os);
            linux = (RadioButton) findViewById(R.id.linux);
            windows = (RadioButton) findViewById(R.id.windows);
            spinner = (Spinner) findViewById(R.id.eventRegister);
            college = (Spinner) findViewById(R.id.collegename);
            firstname = (EditText) findViewById(R.id.fname);
            lastname = (EditText) findViewById(R.id.lname);
            emailid = (EditText) findViewById(R.id.mailid);
            mobno = (EditText) findViewById(R.id.mobno);
            group = (RadioGroup) findViewById(R.id.gender);
            male = (RadioButton) findViewById(R.id.male);
            female = (RadioButton) findViewById(R.id.female);
            mauth = FirebaseAuth.getInstance();
            user = mauth.getCurrentUser();
            eventref = FirebaseDatabase.getInstance().getReference("TechnoTweet");
            UserEntriesref = FirebaseDatabase.getInstance().getReference("UserEntries");
            eventref.keepSynced(true);

        try {
                database = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
                createEmployeeTable();
            }catch(Exception ex)
            {
                Toast.makeText(getApplicationContext(),"Already registered this entry",Toast.LENGTH_LONG).show();
            }
            if(isNetworkAvailable())
            {
                Toast.makeText(getApplicationContext(),"Internet Connection is available",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Check Your Internet Connection",Toast.LENGTH_LONG).show();
            }
            email = mauth.getCurrentUser().getEmail().toString();
            male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    linux.setEnabled(true);
                    windows.setEnabled(true);
                }
            });
            female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    linux.setEnabled(false);
                    windows.setEnabled(false);
                    linux.setChecked(false);
                    windows.setChecked(false);
                }
            });
            amount = (EditText) findViewById(R.id.amount);

            eventType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, EventRegister);
            collegeType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CollegeName);
            eventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            collegeType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(eventType);
            college.setAdapter(collegeType);
            submit = (Button) findViewById(R.id.submit);

            name = email.substring(0, email.indexOf('@'));

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                final String fname, lname, email, mob, Gender,osname, event, College, Amount;
              fname = firstname.getText().toString().trim();
              lname = lastname.getText().toString().trim();
              email = emailid.getText().toString().trim();
              mob = mobno.getText().toString();
              Amount = amount.getText().toString();
              event = spinner.getSelectedItem().toString();
              College = college.getSelectedItem().toString();

              try {
                  if (validateField(fname, lname, email, mob, Amount)) {


                      if (male.isChecked()) {
                          Gender = male.getText().toString();

                      } else {
                          Gender = female.getText().toString();
                      }

                      if (linux.isChecked()) {
                          osname = linux.getText().toString();
                      } else {
                          osname = windows.getText().toString();
                      }

                      if (event.equals("AndroBeatz")) {

                          remainingAmount = AndroAmount - Integer.parseInt(Amount);

                      } else {

                          remainingAmount = WebAmount - Integer.parseInt(Amount);

                      }

                      final String msg = "***WLUG TechoTweet2K18***.\n" +
                              "        Congratulations!!!" + "  " + "Dear" + " " + fname + "  " + lname + "\n" +
                              " You have successfully registered to our TechnoTweet Workshop.\n" +
                              "        Date             : 10th & 11th March 2k18\n" +
                              "        Registration Time: 8:30 am\n" +
                              "        Event name       : " + event + "\n" +
                              "        Amount           : " + Amount + "₹\n" +
                              "        Remaining amount : " + remainingAmount + "₹\n" +
                              "        Entered By       : " + name + "\n" +
                              "        Note             : If possible bring your laptop with you." + "\n" +
                              "        Thank You!!! \n" +
                              "        Contact No      : Piyush Kadam :7875991001 Shubham Deshmukh: 7057978022";

                      if (Integer.parseInt(Amount) < 100) {
                          //dialog.dismiss();
                          if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                          {
                          //    dialog.dismiss();
                              emailid.setError("Invalid Email");
                              emailid.setFocusable(true);
                          }
                          Snackbar.make(view, "Amount Should be greater than or equal to 100", Snackbar.LENGTH_LONG).show();
                      }
                      else  if(Integer.parseInt(Amount)>300)
                      {

                          if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                          {

                              emailid.setError("Invalid Email");
                              emailid.setFocusable(true);
                          }
                          Snackbar.make(view,"Please enter Fixed amount for this Event",Snackbar.LENGTH_LONG).show();
                      }
                      else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                      {

                            emailid.setError("Invalid Email");
                            emailid.setFocusable(true);
                      }
                              else {

                          if(isNetworkAvailable()) {
                              if (event.equals("AndroBeatz")) {
                                  androref = eventref.child("AndroBeatz").child(College);
                                  DatabaseReference data = androref.push();
                                  data.child("First Name").setValue(fname);
                                  data.child("Last Name").setValue(lname);
                                  data.child("Email").setValue(email);
                                  data.child("MobNo").setValue(mob);
                                  data.child("Have Laptop").setValue(Gender);
                                  data.child("OS name").setValue(osname);
                                  data.child("RegisterFor").setValue(event);
                                  data.child("College Name").setValue(College);
                                  data.child("Amount").setValue(Amount);
                                  data.child("Entered by").setValue(name);

                                  //User wise entries
                                  DatabaseReference userentries = UserEntriesref.child(user.getUid()).child("AndroBeatz").push();
                                  userentries.child("firstname").setValue(fname);
                                  userentries.child("lastname").setValue(lname);
                                  userentries.child("email").setValue(email);
                                  userentries.child("mobno").setValue(mob);
                                  //userentries.child("Have Laptop").setValue(Gender);
                                  //userentries.child("OS name").setValue(osname);
                                  userentries.child("eventName").setValue(event);
                                  userentries.child("college").setValue(College);
                                  userentries.child("amount").setValue(Amount);


                                  Snackbar.make(view, "Registered successfully. Thank You!", Snackbar.LENGTH_LONG).show();
                                  SendEmail sm = new SendEmail(MetaForm.this, email, "WLUG", msg);
                                  sm.execute();

                                  firstname.setText(null);
                                  lastname.setText(null);
                                  emailid.setText(null);
                                  mobno.setText(null);
                                  amount.setText(null);


                              } else {

                                  androref = eventref.child("Webino").child(College);
                                  DatabaseReference data = androref.push();
                                  data.child("First Name").setValue(fname);
                                  data.child("Last Name").setValue(lname);
                                  data.child("Email").setValue(email);
                                  data.child("MobNo").setValue(mob);
                                  data.child("Have Laptop").setValue(Gender);
                                  data.child("OS name").setValue(osname);
                                  data.child("RegisterFor").setValue(event);
                                  data.child("College Name").setValue(College);
                                  data.child("Amount").setValue(Amount);
                                  data.child("Entered by").setValue(name);

                                  //User wise entry
                                  DatabaseReference userentries = UserEntriesref.child(user.getUid()).child("Webino").push();
                                  userentries.child("firstname").setValue(fname);
                                  userentries.child("lastname").setValue(lname);
                                  userentries.child("email").setValue(email);
                                  userentries.child("mobno").setValue(mob);
                                  //userentries.child("Have Laptop").setValue(Gender);
                                  //userentries.child("OS name").setValue(osname);
                                  userentries.child("eventName").setValue(event);
                                  userentries.child("college").setValue(College);
                                  userentries.child("amount").setValue(Amount);

                                  Snackbar.make(view, "Registered successfully. Thank You!", Snackbar.LENGTH_LONG).show();
                                                      SendEmail sm = new SendEmail(MetaForm.this, email, "WLUG", msg);
                                                      sm.execute();



                                  firstname.setText(null);
                                  lastname.setText(null);
                                  emailid.setText(null);
                                  mobno.setText(null);
                                  amount.setText(null);

                              }
                          }
                          else {

                            //  Toast.makeText(getApplicationContext(),"Offline entry has been saved",Toast.LENGTH_LONG).show();
                              Snackbar.make(view,"Check your Internet Connection",Toast.LENGTH_LONG).setDuration(5000).show();
                              addEntry(fname,email,mob,event,name,Amount,College);
                              Toast.makeText(getApplicationContext(),"Offline entry has been saved",Toast.LENGTH_LONG).show();

                              firstname.setText(null);
                              lastname.setText(null);
                              emailid.setText(null);
                              mobno.setText(null);
                              amount.setText(null);
                          }
                      }

                      } else{

                          Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_LONG).show();
                      }

                  }
              catch(Exception ex)
                  {
                      //dialog.dismiss();
                      Toast.makeText(getApplicationContext(),"Already registered this entry", Toast.LENGTH_LONG).show();
                  }

            }

        });

}

    public boolean validateField(String fname,String lname,String email,String mob,String Amount)
    {
        if (fname.isEmpty() || lname.isEmpty() || email.isEmpty() || mob.isEmpty() || Amount.isEmpty()) {

            return false;
        }
        else {
            return true;
        }
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void createEmployeeTable() {
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS Technotweet (\n" +
                        "    name varchar(200) NOT NULL,\n" +
                        "    email varchar(200) NOT NULL PRIMARY KEY,\n" +
                        "    mob_no int NOT NULL,\n" +
                        "    Event varchar(200) NOT NULL,\n" +
                        "    EnteredBy varchar(200) NOT NULL,\n" +
                        "    Amount int NOT NULL, \n"+
                        "    CollegeName int NOT NULL \n"+
                        ");"
        );
    }


    public void addEntry(String name,String email,String mob_no,String Event,String EnteredBy,String Amount,String CollegeName)
    {
        String insertSQL = "INSERT INTO Technotweet \n" +
                "(name, email, mob_no, Event,EnteredBy,Amount,CollegeName)\n" +
                "VALUES \n" +
                "(?, ?, ?, ?, ?, ?, ?);";

        database.execSQL(insertSQL,new String[]{name,email,mob_no,Event,EnteredBy,Amount,CollegeName});
    }
}







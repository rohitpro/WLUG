package com.example.shree.wlug;

/**
 * Created by rohit on 4/2/18.
 */

public class UserEntries {

    String firstname,lastname,email,mobno,eventName,amount,college;

    public UserEntries()
    {

    }

    public UserEntries(String firstname, String lastname, String email, String mobno, String eventName, String amount, String college) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.mobno = mobno;
        this.eventName = eventName;
        this.amount = amount;
        this.college = college;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
}

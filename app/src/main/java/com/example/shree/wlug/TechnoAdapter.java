package com.example.shree.wlug;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by rohit on 2/2/18.
 */

public class TechnoAdapter extends ArrayAdapter<TechnoEntry> {

    Context mCtx;
    int listLayoutRes;
    List<TechnoEntry> technoEntries;
    SQLiteDatabase mDatabase;
    public TechnoAdapter(@NonNull Context context, int listLayoutRes, List<TechnoEntry> technoEntries,SQLiteDatabase mDatabase) {

        super(context, listLayoutRes,technoEntries);

        this.mCtx = context;
        this.listLayoutRes = listLayoutRes;
        this.technoEntries = technoEntries;
        mDatabase = mDatabase;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(listLayoutRes, null);

        //getting employee of the specified position
        TechnoEntry employee = technoEntries.get(position);


        //getting views
        TextView textViewName = view.findViewById(R.id.textViewName);
        TextView textViewemail = view.findViewById(R.id.textViewemail);
        TextView textViewmob = view.findViewById(R.id.textMob);
        TextView textViewEvent = view.findViewById(R.id.textEvent);
        TextView textViewEntered = view.findViewById(R.id.textViewEnteredBy);
        TextView textViewAmount = view.findViewById(R.id.textViewAmount);
        TextView textViewCollege = view.findViewById(R.id.textViewCollege);

        textViewName.setText("Name:"+" "+ employee.getName());
        textViewemail.setText("Email Id:"+" "+ employee.getEmail());
        textViewEvent.setText("Event name:"+" "+employee.getEvent());
        textViewmob.setText("Mob No:"+" "+employee.getMob_no());
        textViewEntered.setText("Entered By:"+" "+employee.getEnteredBy());
        textViewAmount.setText("Amount:"+" "+employee.getAmount());
        textViewCollege.setText("College Name:"+" "+employee.getCollegeName());
        return view;
    }
}
